---
layout: markdown_page
title: "Reseller Marketing Kit"
---

## Getting Started

The information contained in this section will provide you with solid understanding of who is GitLab, what do we do and where are we going in the future

* [GitLab narrative and pitch](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit#slide=id.g39d65c7ce1_12_233)
* [GitLab elevator pitch](https://about.gitlab.com/handbook/marketing/product-marketing/#elevator-pitch)
* [GitLab product vision](https://about.gitlab.com/direction/product-vision/)
* [Value of selling GitLab Ultimate](https://about.gitlab.com/pricing/ultimate/)
* [GitLab personas](https://about.gitlab.com/handbook/marketing/product-marketing/#gitlab-personas)

## Marketing Resources

* [GitLab boilerplate web content](https://docs.google.com/document/d/1jzC2l88sKPDUWSXEgae4tqAg_QR34RDl6mPN5V8a0Mw/edit?usp=sharing)
* [GitLab official logo, guidelines and other artwork](https://about.gitlab.com/press/) - scroll all the way down to the *Images & Logos* section
* Reseller marketing issue board (coming soon)

## Frequently Asked Questions

1. Can one organization implement Core, Starter, Premium and Ultimate at the same time?
1. What is the difference in GitLab Ultimate and GitLab Gold?
