(function() {
  var nav = document.getElementById('main-nav');
  function changeNavClass() {
    if (nav.offsetTop - window.scrollY <= 1) {
      // remove navbar-header-dark class
      nav.classList.remove('navbar-header-dark');
    } else {
      // add it back
      nav.classList.add('navbar-header-dark');
    }
  }
  // only give the window a scroll function if the header is dark
  if (nav.classList.contains('navbar-header-dark')) {
    window.addEventListener('scroll', changeNavClass);
  }
})();
