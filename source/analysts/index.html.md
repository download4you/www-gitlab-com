---
layout: markdown_page
title: Analyst Relations at GitLab
description: Insight about AR at GitLab
---

## What are analysts saying about GitLab and how are we building off their insight and input?

## Where does GitLab Engage?

GitLab appears in several areas within the analyst arena.  Areas we engage analysts include:

  - Source Code Management (SCM)
  - Release Automation
  - Continuous Integration and Continuous Delivery (CI/CD)
  - Value Stream Management (VSM)
  - Application Security (AppDevSec)
  - Continuous Deployment and Release Automation (CDRA)
  - DevOps
  - Planning/Project Management
  - Agile Project management

## GitLab evaluated in recent reports

Here are links to recent reports where GitLab the company and/or the product has been covered or evaluated.

  - [The Forrester Wave™: Continuous Integration Tools, Q3 2017](https://reprints.forrester.com/#/assets/2/921/RES137261/reports)
  - [The Forrester New Wave™: Value Stream Management Tools, Q3 2018](https://reprints.forrester.com/#/assets/2/921/RES141538/reports)
  - IDC Innovators: Agile Code Development Technologies 2018   

## GitLab mentioned in recent reports

Here are links to reports where GitLab is mentioned within the greater report discussion. (Note: full report may not be available)

### Forrester

  - [Evolve Or Retire: Administrators Are Now Developers, July 13, 2018](https://www.forrester.com/report/Evolve+Or+Retire+Administrators+Are+Now+Developers/-/E-RES137184)
  - [Elevate Agile-Plus-DevOps With Value Stream Management, May 11, 2018](https://www.forrester.com/report/Elevate+AgilePlusDevOps+With+Value+Stream+Management/-/E-RES142463)
  - [The State of Application Security, 2018, January 23, 2108](https://www.forrester.com/report/The+State+Of+Application+Security+2018/-/E-RES141676)
  - [The Quest For Speed-Plus-Quality Drives Agile And DevOps Tool Selection, April 17, 2017](https://www.forrester.com/report/The+Quest+For+SpeedPlusQuality+Drives+Agile+And+DevOps+Tool+Selection/-/E-RES122555)
  - [Cloud-Based DevOps Tools Are Ripening Quickly, April 4, 2017](https://www.forrester.com/report/CloudBased+DevOps+Tools+Are+Ripening+Quickly/-/E-RES137388)

## GitLab open issues

TBD