# Categories that GitLab has offerings in. Derived from https://about.gitlab.com/handbook/product/categories

# Manage

# cycle_analytics:
#   name: "Cycle analytics"
#   stage: manage
#   alt_link: https://docs.gitlab.com/ee/user/project/cycle_analytics.html
#   description: "Measure so you can mange. Cycle analytics give you data on how fast you are cycling and what steps are slowing you down so you can optimize."
#
# devops_score:
#   name: "DevOps Score"
#   stage: manage
#   alt_link: https://docs.gitlab.com/ee/user/instance_statistics/convdev.html
#   description: "Get an overview of how well your organization is adopting DevOps and to see the impact on your velocity."
#
# audit_logs:
#   name: "Audit logs"
#   stage: manage
#   alt_link: https://docs.gitlab.com/ee/administration/audit_events.html
#   description: "Stay compliant with a full audit trail of user events. Troubleshoot recent changes in a project or get a global view of your system."

reporting_analytics:
  name: "Reporting and Analytics"
  stage: manage
  description: "Manage the software value stream process to identify and remove friction."
  link: https://gitlab.com/users/sign_in
  link_description: "Sign up for GitLab.com"
  body:  |
    ## Manage and optimize your value stream

    Leverage the data from your delivery process to streamline and accelerate business innovation.

    ### Value stream management

    * The value streams help you visualize and manage the flow of new innovation from idea to customers.
    * Drive continuous improvement based on the data from your value stream.
    * Identify and remove constraints and bottlenecks that create friction.

    ### Cycle Analytics

    *  Monitor and measure the end process / cycle time for lifecycle stages
    *  Improve and accelerate delivery.

    ### Compliance and auditing

    *  Produce reports and documentation to support audits
    *  Improve overall project compliance and visiblity

# Plan

project_management:
  name: "Project Management"
  description: "Collaboratively manage projects and teams to plan and deliver software at DevOps speed."
  link: https://gitlab.com/users/sign_in
  link_description: "Sign up for GitLab.com"
  body: |
      ## Product manager and developer collaboration

      Do more than manage and maintain lists of issues. Quickly view and manage the status, assignee or milestone for multiple issues at the same time or easily filter them on any properties. A common team interface makes it easy to organize, plan and lead delivery teams to accelerate delivery.

      ### Collaborate

      * Improve team efficiency with one tool to track issues, code, and delivery.
      * Enable discussions about epics, issues and code changes.
      * Shift left design, testing, and security into one conversation.
      * Support your customers, who can email bug reports, feature requests, or any other general feedback into your GitLab project.

      ### Track

      * Track status of development and delivery with Kanban, Sprints, and CI / CD pipeline visibility.
      * Understand issues and obstacles in the discussions.
      * Track issue status with flexible team, project and group boards.
      * Burndown charts, epics, and roadmaps to stay on plan.

      ### Deliver

      * Align team and ship code faster with a single application.
      * Ensure compliance with built in security scanning and license management.
      * Increase quality with built in testing, code quality and code reviews.
      * Robust wiki based project documentation.

  stage: plan

portfolio_management:
  name: "Portfolio Management"
  description: "Align strategy with execution by planning and tracking projects and portfolios."
  link: https://gitlab.com/users/sign_in
  link_description: "Sign up for GitLab.com"
  body: |
    ## Plan and manage the interaction between multiple projects

    ### Strategic Alignment

    * Prioritize epics to ensure alignment.
    * Identify cross-project relationships.
    * Track status and delivery.


    ### Optimize flow

    * Capture end to end value stream metrics.
    * Identify and resolve obstacles.
    * Accelerate project delivery to meet business demand.

    ### Manage portfolio delivery

    * Plan delivery with clear milestones and roadmaps.
    * Communicate status and future changes.
    * Manage capacity and resources.

  stage: plan

  service_desk:
    name: "Service desk"
    stage: plan
    alt_link: https://docs.gitlab.com/ee/user/project/service_desk.html
    description: "Allows your team to connect directly with any external party through email right inside of GitLab with no external tools required."

# Create

scm:
  name: "Source Code Management"
  description: "Manage source code changes enabling coordination, sharing and collaboration across the entire software development team.   Track and merge branches, audit changes and enable concurrent work, to accelerate software delivery."
  link: https://gitlab.com/users/sign_in
  link_description: "Sign up for GitLab.com"
  body: |
    ## Software development team collaboration

    Enable your development team to maximize their productivity with world class source code management.

    ### Collaborate

    * Review, comment, and improve each other’s code.
    * Share code, enable re-use and  ‘innersourcing’.
    * File locking prevents conflicts.
    * Robust WebIDE accelerates development on any platform.

    ### Accelerate

    * Git based repository, enabling developers to work from a local copy.
    * Branch and merge code.
    * Built-in CI and CD streamlines testing and delivery.

    ### Secure

     * Scan for code quality and security with every commit.
     * Granular access controls.

  stage: create

code_review:
  name: "Code Review"
  description: "Review code, discuss changes, share knowledge, and identify defects in code among distributed teams via asynchronous review and commenting. Automate, track and report code reviews."
  link: https://gitlab.com/users/sign_in
  link_description: "Sign up for GitLab.com"
  body: |
    ## Streamline code review and approvals

    Enable your development team to collaborate, review and improve their code.

    ### Collaborate

    * Review, comment, and improve each other’s code.
    * Approval workflow enables reviewing and approving changes.
    * Inline comments enable asynchronous review and feedback.

  stage: create

web_ide:
  name: "Web IDE"
  stage: create
  alt_link:  https://docs.gitlab.com/ee/user/project/repository/web_editor.html
  description: "A full featured integraded development environment built in to GitLab so you can start contributing on day one with no need to spend days geting all the right packages installed into your local dev environment."

wiki:
  name: "Wiki"
  stage: create
  alt_link: https://docs.gitlab.com/ee/user/project/wiki/
  description: "Share documentation and organization information with a built in wiki."

# Verify

ci:
  name: "Continuous Integration (CI)"
  description: "Gain the confidence to ship at blistering speed and immense scale with automated builds, testing, and out-of-the-box security to verify each commit moves you forward."
  stage: verify
  body: |
    ![GitLab CI/CD](/images/ci/ci-cd-test-deploy-illustration_2x.png)

    ## Simple to use

      - GitLab CI comes built-in to GitLab's single DevOps application for a seamless epxerience.
      - Developers can self-service and easily collaborate with QA teams by working from verionable, configuration YAMl file.
      - CI results appear together with code and comments in single place so you get one conversation thread with visibility into all the info you need to review, troubleshoot, and improve faster.
      - Seemless experience together with [GitLab CD release automation](/product/cd).

    ## Rated #1 in the Forrester CI Wave™

    ![Forrester CI Wave](/images/home/forrester-ci-wave-graphic-with-arrow.svg)

    “GitLab supports development teams with a well-documented installation and configuration processes, an easy-to-follow UI, and a flexible per-seat pricing model that supports self service. GitLab’s vision is to serve enterprise-scale, integrated software development teams that want to spend more time writing code and less time maintaining their tool chain.” - Forrester CI Wave™

    [Learn more](/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/){: .btn .cta-btn .accent}

    ## Powerful capabilities

      - **Multi-platform**: Execute builds on Unix, Windows, macOS, and any other platform that supports Go.
      - **Multi-language**: Build scripts are command line driven and work with Java, PHP, Ruby, C, and any other language.
      - **Stable**: Builds run on a different machine than GitLab.
      - **Parallel builds**: GitLab CI/CD splits builds over multiple machines, for fast execution.
      - **Realtime logging**: A link in the merge request takes you to the current build log that updates dynamically.
      - **Versioned tests**: A [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/) file that contains your tests, allowing everyone to contribute changes and ensuring every branch gets the tests it needs.
      - **Pipeline**: Define multiple jobs per stage and you can [trigger other builds](https://docs.gitlab.com/ee/ci/triggers/).
      - **Autoscaling**: [Automatically spin up and down VM's](/2016/03/29/gitlab-runner-1-1-released/) to make sure your builds get processed immediately and minimize costs.
      - **Build artifacts**: Upload binaries and other build artifacts to GitLab and browse and download them.
      - **Test locally**: [Multiple executors](https://docs.gitlab.com/runner/executors/) let you [reproduce tests locally](https://gitlab.com/gitlab-org/gitlab-runner/issues/312).
      - **Docker support**: Use custom Docker images, spin up [services](https://docs.gitlab.com/ee/ci/services/) as part of testing, [build new Docker images](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html), even run on [Kubernetes](/kubernetes/).

    ## More information

      - Read how to use GitLab CI in the [GitLab CI/CD documenation](https://docs.gitlab.com/ee/ci/)
      - View a [demo of GitLab CI/CD](https://www.youtube.com/watch?v=1iXFbchozdY)
      - [HumanGeo switched from Jenkins to GitLab and cut costs by 1/3](/2017/11/14/humangeo-switches-jenkins-gitlab-ci/)
      - [Ticketmaster sped up build times by 15x with GitLab CI](/2017/06/07/continous-integration-ticketmaster/)
      - View a step-by-step guide to [migrate from Jenkins to GitLab](https://www.youtube.com/watch?v=RlEVGOpYF5Y)
      - [Autoscale GitLab CI runners and save 90% on EC2 costs](/2017/11/23/autoscale-ci-runners/)

unit_testing:
  name: "Unit Testing"
  alt_link: https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#unit-tests
  stage: verify
  description: "Ensure that a single unit of code (a method) works as expected (given an input, it has a predictable output)."

integration_testing:
  name: "Integration Testing"
  alt_link: https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#integration-tests
  stage: verify
  description: "Tests that ensure individual parts of the application work well together, without the overhead of the actual app environment (i.e. the browser)"

# acceptance_testing: # no docs or content on acceptance testing
#   name: "Acceptance Testing"
#   alt_link:
#   stage:
#   description:

code_quality:
  name: "Code Quality"
  alt_link: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
  stage: verify
  description: "Automatically analyze your source code to surface issues and see if quality is improving or getting worse with the latest commit."

# package

container_registry:
  name: "Container Registry"
  stage: package
  alt_link: https://docs.gitlab.com/ee/user/project/container_registry.html
  description: "GitLab Container Registry is a secure and private registry for Docker images. Built on open source software, GitLab Container Registry isn't just a standalone registry; it's completely integrated with GitLab.  Easily use your images for GitLab CI, create images specific for tags or branches and much more."


# release

cd:
  name: "Continuous Delivery (CD)"
  alt_link: https://about.gitlab.com/features/gitlab-ci-cd/
  stage: release
  description: "Code changes are automatically built, tested, and prepared for a release to a testing environment and/or a production environment after the build stage.  This enables many production deployments every day. Can you imagine having Continuous Integration, Continuous Delivery, and Continuous Deployment within the same web interface? With GitLab, you can!"

#release_automation: # no docs or content on this

pages:
  name: "Pages"
  stage: release
  alt_link: https://about.gitlab.com/features/pages/
  description: "Use any static site genertor to create websites that are easily managed and deployed by GitLab"

review_apps:
  name: "Review apps"
  stage: release
  alt_link: https://about.gitlab.com/features/review-apps/
  description: "Get a full prodution like environment for every merge request that updates on each commit. See code running and enable user accepance testing before you merge."

# Configure

auto_devops:
  name: "Auto DevOps"
  stage: configure
  alt_link: https://about.gitlab.com/auto-devops/
  description: "Commit your code and GitLab does the rest to build, test, deploy, and monitor automatically. Eliminate the complexities of getting going with automated software delivery by automatically setting up the pipeline and necessary integrations, freeing up your teams to focus on the culture part."

# infrastructure_configuration: # no docs or issues for this
#   name: "Infrastructure Configuration"
#   description: "GitLab configuration management allows you to manage your software delivery pipeline from a single place. GitLab has Auto DevOps that creates your entire pipeline and monitoring configuration for you. Get started in minutes, not months. Then GitLab configuration is fully customizable so you can tailor it to meet your needs."
#   stage: configure

chatops:
  name: "ChatOps"
  stage: configure
  alt_link: https://docs.gitlab.com/ee/ci/chatops/
  description: "Tight integrations with Slack and Mattermost make it easy to manage and automate software development and delivery right from your chat app."

# Monitor

metrics:
  name: "Metrics"
  alt_link: https://docs.gitlab.com/ee/administration/monitoring/prometheus/gitlab_metrics.html
  stage: monitor
  description: "Monitor your GitLab instance to ensure your deployment is running optimally."

apm:
  name: "Application Performance Monitoring (APM)"
  alt_link: https://docs.gitlab.com/ee/administration/monitoring/performance/
  stage: monitor
  description: "Monitor the performance and status of your applications. GitLab Application Performance Monitoring makes it possible to measure a wide variety of statistics, visualized in graphs and dashboards. Infrastructure monitoring includes public and private whitebox infrastructure monitoring and also blackbox infrastructure monitoring. Network, System, and Application logs are processed, stored, and searched for investigating errors and incidents such as application activity, spam events, transient errors, system and network authentication events, security events, and more."

infrastructure_monitoring:
  name: "Infrastructure Monitoring"
  alt_link: https://docs.gitlab.com/ee/user/project/clusters/#monitoring-your-kubernetes-cluster
  description: "Out-of-the-box Kubernetes cluster monitoring let you know the health of your deployment environemnts with traceability back to every issue and code change as part of a single application for end-to-end DevOps."
  stage: monitorfc


# Secure

application_security_testing:
  name: "Application Security Testing"
  stage: secure
  description: "As part of Concurrent Devops, GitLab offers powerful security tools out of the box including static and dynamic application security testing, container and open source dependency scanning and license management. Test every merge request for security vulnerabilities so you can find mistakes earlier when they are less costly to resolve."
  link: https://gitlab.com/users/sign_in
  link_description: "Sign up for GitLab.com"
  body: |
    ## Fundamentally different

    * Security issues reported directly in Pipelines and Merge Requests.
    * One license cost for integrated security.
    * Zero context switching to proactively secure applications.

    ### Static Application Security Testing

    * Performs static analysis on application source code and binaries.
    * Spots potential vulnerabilities before deployment.
    * Uses open source (defined on right) tools that are installed as part of GitLab.
    * Vulnerabilities shown in-line with merge request.
    * Results collected and presented as a single report.

    ### Dependency scanning

    * Analyzes external dependencies (e.g. libraries like Ruby gems).
    * Shows vulnerable dependencies needing updating.
    * Relies on open source tools and on the integration with Gemnasium technology (now part of GitLab).
    * Vulnerabilities shown in-line with merge request.
    * Results collected and available as a single report.

    ### Container scanning

    * Performs static analysis on Docker images to spot possible vulnerabilities in the application environment.
    * Analyzes image contents against public vulnerability databases.
    * Uses open source tool Clair that is able to scan any kind of Docker (or Appc) image.
    * Vulnerabilities shown in-line with merge request.

    ### Dynamic Application Security Testing

    * Analyzes your running web application for known runtime vulnerabilities.
    * Runs live attacks against a Review App* of the application.
    * Users can provide HTTP credentials to test private areas.
    * Current implementation uses open source tool ZAProxy, modified to add authentication capabilities.
    * Vulnerabilities shown in-line with merge request.

    ### [License Management](https://about.gitlab.com/product/license-management)

license_management:
  name: "License Management"
  stage: secure
  description: "Ensure the software licenses of your application's dependencies are compatible with your license policies. See what licenses your project uses in its dependencies, and decide for each of them whether to allow it or forbid it."
  link: https://gitlab.com/users/sign_in
  link_description: "Sign up for GitLab.com"
  body: |
    ## Dependencies license management with GitLab CI/CD

    * Search project dependencies for approved and blacklisted licenses
    * Define custom license policies per project
    * Show software licenses being used and identify if they are not within policy
    * Use open source tool LicenseFinder
    * See license analysis results in merge request for immediate resolution

